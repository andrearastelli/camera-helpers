import sys

import maya.api.OpenMaya as OpenMaya


kCommandName = 'cameraFitToGeo'


def maya_useNewAPI():
    pass


class CameraFitToGeo(OpenMaya.MPxCommand):

    kSelectedCameraFlag = '-c'
    kSelectedCameraLongFlag = '-camera'

    def __init__(self):
        super(CameraFitToGeo, self).__init__()

    @staticmethod
    def creator():
        return CameraFitToGeo()

    @staticmethod
    def setSyntax():
        syntax = OpenMaya.MSyntax()
        syntax.addFlag(CameraFitToGeo.kSelectedCameraFlag, CameraFitToGeo.kSelectedCameraLongFlag, OpenMaya.MSyntax.kString)

        return syntax

    def doIt(self, args):
        argDb = OpenMaya.MArgDatabase(self.syntax(), args)

        if argDb.isFlagSet(CameraFitToGeo.kSelectedCameraFlag):
            print argDb.flagArgumentString(CameraFitToGeo.kSelectedCameraFlag, 0)

    def redoIt(self):
        pass

    def undoIt(self):
        pass

    def isUndoable(self):
        pass


def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "Any")

    try:
        mplugin.registerCommand(kCommandName, CameraFitToGeo.creator, CameraFitToGeo.setSyntax)
    except:
        sys.stderr.write("Failed to register command %s\n" % kCommandName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterCommand(kCommandName)
    except:
        sys.stderr.write("Failed to deregister command %s\n" % kCommandName)
        raise
